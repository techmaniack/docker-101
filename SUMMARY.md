# Summary

* [1. Installation](chapter-1/README.md)
  * [Docker installation](chapter-1/docker_installation.md)
  * [Docker Compose installation](chapter-1/docker_compose_installation.md)

* [2. Hello World Container](chapter-2/README.md)
  * [Run a hello world container](chapter-2/hello_world.md)
  * [CRUD containers](chapter-2/crud_containers.md)  

* [3. Rest Of The World Containers](chapter-3/README.md)
  * [MySQL](chapter-3/mysql.md)
  * [Multiple versions of MySQL containers](chapter-3/multiple_mysql.md)
  * [Persisting data using Volumes](chapter-3/persistence.md)

* [4. Dockerfile](chapter-4/README.md)
  * [Create a docker image to serve phpinfo()](chapter-4/dockerfile.md)
  * [CRUD images](chapter-4/crud_images.md)
  
* [5. Composing Environments](chapter-5/README.md)
  * [Deploy Wordpress locally](chapter-5/wordpress.md)



  <!-- * [Docker Compose](chapter-4/README.md) -->
<!--   * [Simplest compose file](chapter-4/simplest.md) -->
<!--   * [Rails Application](chapter-4/rails.md) -->
<!--   * [MySQL Cluster](chapter-4/mysql_cluster.md) -->
<!--   * [Voting Application](chapter-4/voting.md) -->

<!-- * [Docker Registry](chapter-5/README.md -->
<!--   * [Local registry setup](chapter-5/local_setup.md) -->

<!-- * [Docker Swarm](chapter-6/README.md) -->
<!--   * [Setting up swarm cluster](chapter-6/setup.md) -->
