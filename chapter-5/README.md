## Objective
Setup a local development environment for Wordpress. Use docker compose and create separate containers for php and MySQL.

### Working Directory
~/docker101/wordpress

### Files
~/docker101/wordpress/docker-compose.yml

